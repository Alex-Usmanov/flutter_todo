class Todo{
  String _title;
  String _description;
  Urgency _urgency;
  bool _isDone;

  Todo(String title, String description, Urgency urgency, bool isDone){
    _title = title;
    _description = description;
    _urgency = urgency;
    _isDone = isDone;
  }

  void toggleDoneStatus(){
    _isDone = !_isDone;
  }


  String get getTitle => _title;

  bool get isDone => _isDone;

  set setDoneStatus(bool value) {
    _isDone = value;
  }

  Urgency get getUrgency => _urgency;

  set setUrgency(Urgency value) {
    _urgency = value;
  }

  String get getDescription => _description;

  set setDescription(String value) {
    _description = value;
  }


  static List<Todo> getTodosByUrgency(List<Todo> allTodos, Urgency urgency){
    List<Todo> result = new List<Todo>();
    for (Todo todo in allTodos){
      if (todo.getUrgency == urgency)
        result.add(todo);
    }
    return result;
  }

}

enum Urgency{
  Urgent,
  Medium,
  Little
}

int urgencyToInt(Urgency urgency){
  if (urgency == Urgency.Urgent)
    return 1;
  else if (urgency == Urgency.Medium)
    return 2;
  else if (urgency == Urgency.Little)
    return 3;
}

Urgency intToUrgency(int index){
  if (index == 0)
    return Urgency.Urgent;
  else if (index == 1)
    return Urgency.Medium;
  else if (index == 2)
    return Urgency.Little;
}