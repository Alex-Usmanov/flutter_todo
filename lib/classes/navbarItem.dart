import 'package:flutter/material.dart';
import 'package:todoapp/classes/todo.dart';

class NavigationBarItem{
  IconData _iconData;
  bool _isActive;
  Urgency _urgency;

  Urgency get getUrgency => _urgency;

  set setUrgency(Urgency value) {
    _urgency = value;
  }

  NavigationBarItem(IconData icon, bool isActive, Urgency urgency){
    _iconData = icon;
    _isActive = isActive;
    _urgency = urgency;
  }

  IconData get iconData => _iconData;

  set iconData(IconData value) {
    _iconData = value;
  }

  bool get isActive => _isActive;

  set setActiveStatus(bool value) {
    _isActive = value;
  }
}