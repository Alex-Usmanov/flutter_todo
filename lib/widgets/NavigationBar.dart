//import 'package:flutter/material.dart';
//import 'package:todoapp/classes/navbarItem.dart';
//import 'package:todoapp/classes/todo.dart';
//
//
//class NavigationBar extends StatefulWidget {
//  @override
//  _NavigationBarState createState() => _NavigationBarState();
//}
//
//class _NavigationBarState extends State<NavigationBar> {
//  NavigationBarItem _active = navbarItems[0];
//
//  List<Widget> buildNavigationBarItems() {
//    List<Widget> result = new List<Widget>();
//    for (NavigationBarItem item in navbarItems) {
//      result.add(IconButton(
//        icon: Icon(item.iconData),
//        color: item == _active ? Colors.blue : Colors.grey,
//        onPressed: () {
//          setState(() {
//            _active = item;
//          });
//        },
//      ));
//    }
//    return result;
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      height: 60,
//      decoration: BoxDecoration(
//        borderRadius: BorderRadius.only(
//          topLeft: Radius.circular(20.0),
//          topRight: Radius.circular(20.0),
//        ),
//      ),
//      child: Row(
//        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//        children: buildNavigationBarItems(),
//      ),
//    );
//  }
//}
