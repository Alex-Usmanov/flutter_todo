import 'package:flutter/material.dart';
import 'package:todoapp/classes/todo.dart';

class DetailedTodo extends StatefulWidget {
  Todo _todo;

  DetailedTodo(Todo todo) {
    _todo = todo;
  }

  @override
  _DetailedTodoState createState() => _DetailedTodoState(_todo);
}

class _DetailedTodoState extends State<DetailedTodo> {
  Todo _todo;

  _DetailedTodoState(Todo todo) {
    _todo = todo;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_todo.getTitle),
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0,
      ),
      body: Container(
        color: Theme.of(context).primaryColor,
        child: Container(
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20.0),
                topRight: Radius.circular(20.0),
              ),
              color: Colors.white,
            ),
      ),
    ));
  }
}
