import 'package:flutter/material.dart';
import 'package:todoapp/classes/todo.dart';
import 'package:todoapp/widgets/AllTodos.dart';
import 'package:todoapp/classes/navbarItem.dart';
import 'package:todoapp/widgets/NavigationBar.dart';

import 'DetailedTodo.dart';

List<NavigationBarItem> navbarItems = [
  NavigationBarItem(Icons.priority_high, true, Urgency.Urgent),
  NavigationBarItem(Icons.file_download, false, Urgency.Medium),
  NavigationBarItem(Icons.low_priority, false, Urgency.Little),
];

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  Urgency currentUrgency = Urgency.Urgent;
  final pageController = PageController(
    initialPage: 0,
  );

  List<Todo> todos = [
    Todo("Example #4", "Feed your doggo", Urgency.Medium, false),
    Todo("Example #5", "Feed your doggo", Urgency.Medium, false),
    Todo("Example #6", "Feed your doggo", Urgency.Medium, false),
    Todo("Example #1", "Feed your doggo", Urgency.Little, false),
    Todo("Example #2", "Feed your doggo", Urgency.Little, false),
    Todo("Example #3", "Feed your doggo", Urgency.Little, false),
    Todo("Example #7", "Feed your doggo", Urgency.Urgent, false),
    Todo("Example #8", "Feed your doggo", Urgency.Urgent, false),
    Todo("Example #9", "Feed your doggo", Urgency.Urgent, false),
  ];

  Widget buildListTile(BuildContext context, int index) {
    Todo todo = Todo.getTodosByUrgency(todos, currentUrgency)[index];
    return ListTile(
      leading: Checkbox(
        activeColor: Theme.of(context).primaryColor,
        checkColor: Colors.white,
        onChanged: (bool current) {
          setState(() {
            todos[todos.indexOf(todo)].toggleDoneStatus();
          });
        },
        value: todo.isDone,
      ),
      title: Text(todo.getTitle),
      subtitle: Text(todo.getDescription),
      trailing: IconButton(
        icon: Icon(Icons.keyboard_arrow_right),
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => DetailedTodo(
                      Todo.getTodosByUrgency(todos, currentUrgency)[index])));
        },
      ),
    );
  }

  List<Widget> buildNavigationBarItems() {
    List<Widget> result = new List<Widget>();
    for (NavigationBarItem item in navbarItems) {
      result.add(IconButton(
        icon: Icon(item.iconData),
        color: item.getUrgency == currentUrgency ? Colors.blue : Colors.grey,
        onPressed: () {
          setState(() {
            currentUrgency = item.getUrgency;
          });
          int pageIndex = navbarItems.indexOf(item);
          if (pageController.hasClients) {
            pageController.animateToPage(pageIndex,
                duration: const Duration(milliseconds: 400),
                curve: Curves.decelerate);
          }
        },
      ));
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Todo'),
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0.0,
      ),
      bottomNavigationBar: Container(
        height: 60,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.0),
            topRight: Radius.circular(20.0),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: buildNavigationBarItems(),
        ),
      ),
      body: Container(
        color: Theme.of(context).primaryColor,
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20.0),
              topRight: Radius.circular(20.0),
            ),
            color: Colors.white,
          ),
          child: PageView(
            onPageChanged: (int index) {
              setState(() {
                currentUrgency = intToUrgency(index);
              });
            },
            scrollDirection: Axis.horizontal,
            controller: pageController,
            children: <Widget>[
              Container(
                child: ListView.builder(
                  itemBuilder: buildListTile,
                  itemCount:
                      Todo.getTodosByUrgency(todos, Urgency.Urgent).length,
                ),
              ),
              Container(
                color: Colors.white,
                child: ListView.builder(
                  itemBuilder: buildListTile,
                  itemCount:
                      Todo.getTodosByUrgency(todos, Urgency.Medium).length,
                ),
              ),
              Container(
                child: ListView.builder(
                  itemBuilder: buildListTile,
                  itemCount:
                      Todo.getTodosByUrgency(todos, Urgency.Little).length,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
