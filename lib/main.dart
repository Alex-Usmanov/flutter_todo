import 'package:flutter/material.dart';
import 'package:todoapp/widgets/MainScreen.dart';

void main() {
  runApp(MaterialApp(
    home: MainScreen(),
    debugShowCheckedModeBanner: false,
    theme: ThemeData(
      primaryColor: Colors.deepPurple,
      accentColor: Colors.deepPurple,
    ),
  ));
}